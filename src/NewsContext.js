import React, { createContext, useEffect, useState } from "react";
import axios from "axios";



export const NewsContext = createContext();

export const NewsContextProvider = (props) => {
  const [data, setData] = useState();
 const apiKey = "YOUR API KEY";


  useEffect(() => {
    axios
      .get(
        `http://newsapi.org/v2/everything?q=bitcoin&from=2020-10-11&sortBy=publishedAt&apiKey=${apiKey}`
      )
      .then((response) => setData(response.data))
      .catch((error) => console.log(error));
  }, []);

  return (
    <NewsContext.Provider value={{ data }}>
      {props.children}
    </NewsContext.Provider>
  );
};
